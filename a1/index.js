/*
    gitbash > npm init 
    gitbash > npm install express
    .gitignore > node_modules


    1. Create a GET route that will access the "/home" route
        1.1 It should print the message: 'Welcome to the home page'
        1.2 Check the results on Postman
        1.3 Screenshot the result
        1.4 Make lagay the screenshot in a1 folder

    2. Create a GET route that will access the "/users"
        2.1 It should print the users 
        2.2 Check the result on Postman
        2.3 Screenshot the result
        2.4 Make lagay the screenshot in a1 folder

    // STRETCH GOAL
    3. Create a delete route that will access the "/delete-user" route
        3.1 It should print "The user has been deleted"
        3.2 Check the result on Postman
        3.3 Screenshot the result
        3.4 Make lagay the screenshot in a1 folder
*/

//ACTIVITY SOLUTION:

const express = require("express");

const app = express()

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));


//1. Create a GET route that will access the "/home" route

	app.get('/home', (request, response) => {
	response.send('Welcome to Home Page!')
})

//PRE-REGISTER FOR NUMBER 2 GET USERS

	let users = []

	/*
		FOR POSTMAN

		URI: /register
		Method: 'POST'
		body: raw + json
			{
       			"username" : 'johndoe', 
        		"password" : 'johndoe123'
    		},
    		{
        		"username" : 'joy', 
       			"password" : 'joy123'
    		}
	*/

	app.post('/register', (request, response) => {

		if (request.body.username !== '' && request.body.password !== '') {
			users.push(request.body)
			response.send(`User ${request.body.username} successfully registered!`)
			console.log(request.body)
		} else {
			response.send(`Please input BOTH username and password`)
		}
})


//2. Create a GET route that will access the "/users"

	app.get('/users', (request, response) => {
		response.send(users)
	})

//3. Create a delete route that will access the "/delete-user" route

/*
	FOR POSTMAN:
	url: http://localhost:3000/delete-user
	method: DELETE
	body: raw + json
		{
        "username" : 'johndoe', 
        "password" : 'johndoe123'
        }
*/

	app.delete("/delete-user", (request, response) => {

	let message;

	for (let i=0; i<users.length; i++) {

		if (request.body.username == users[i].username) {
			users.pop(request.body)
			message = `User ${request.body.username} has been deleted.`
			break;
		} else {
			message = "User does not exist"
		}
	}
	response.send(message);
})


app.listen(port, () => console.log(`Server running at port ${port}`))


