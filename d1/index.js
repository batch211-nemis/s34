/*
	- Use the "require" directive to load the express module/package.
	-A module is a software component or part of a program that contains one or more routines.
	- This is used to get the contents of the package to be used by our application.
	- It also allows us to access methods and functions that will allows us to easily create a server.
*/

const express = require("express");

/*
	-Create an application using express
	-This creates an express application and stores this in a constant called app
	- In layman's term, app is our server.
*/

const app = express()

//For our application server to run, we need a port to listen top

const port = 3000;

//MIDDLEWARES

/*
	- Set up for allowing the server to handle data from requests
	- Allows your app to read json data
	- Methods used from express.js are middlewares
	- Middleware is a layer of software that enables interaction and transmission of information between assorted applications
*/

app.use(express.json());

/*
	- Allows your app to read data from forms
	- By default, information received from the URL can only be received as a string or an array
	- By applying the option of "extended:true", we are allowed to receive information in other data types such as an object throughout our application.
*/
app.use(express.urlencoded({extended: true}));

//ROUTES
/*
	- Express has methods corresponding to each HTTP method
	- The full base URL for our local application for this route will be at "http://localhost:3000/"
*/


//RETURNS A SIMPLE MESSAGE
/*
	- This route expects to receive a GET request at the base URI "/"

	POSTMAN:
	url: http://localhost:3000/
	method:GET
*/

app.get('/', (request, response) => {
	response.send('Hello World')
})

//RETURNS A SIMPLE MESSAGE
/*
	- This route expects to receive a GET request at the base URI "/"

	POSTMAN:
	URI: /hello
	Method:'GET'

	url: http://localhost:3000/hello
	method:GET
*/

app.get('/hello', (request, response) => {
	response.send('Hello from the "/hello" endpoint')
})

//RETURNS A SIMPLE GREETING
/*
	- This route expects to receive a GET request at the base URI "/"

	POSTMAN:
	URI: /hello
	Method:'POST'

	url: http://localhost:3000/hello
	method: POST
	body: raw + json
		{
			"firstname": "Joy",
			"lastname": "Nemis"
		}
*/

app.post('/hello', (request, response) => {
	response.send(`Hello there,' ${request.body.firstname} ${request.body.lastname}! This is from the "/hello" endpoint but with a post method`)
})

//REGISTER USER ROUTE

let users = []

/*
	- This route expects to receive a POST request at the URI "/register".
	- This will create a user object in the "users" variable that mirrors a real world registration process.

	URI: /hello
	Method: 'POST'
	body: raw + json
		{
			"username": "Joy",
			"password": "password"
		}
*/

app.post('/register', (request, response) => {

		if (request.body.username !== '' && request.body.password !== '') {
			users.push(request.body)
			response.send(`User ${request.body.username} successfully registered!`)
			console.log(request.body)
		} else {
			response.send(`Please input BOTH username and password`)
		}
})

//CHANGE PASSWORD ROUTE

/*
	- This route expects to receive a PUT request at the URI "/change-password"
	- This will update the password of a user that matches the indformation provided in the client/postman
	
	URI: /change-password
	Method: 'PUT'

	POSTMAN:
	url: http://localhost:3000/change-password
	method: PUT
	body: raw + json
		{
			"username": "Joy",
			"password": "password2"
		}
*/

app.use("/change-password", (request, response) => {

	//creates a variable to store the message to be sent back to the client/Postman
	let message;

	//cretaes a for loop through the elements of the "users" array
	for (let i=0; i<users.length; i++) {

		//If the username provided in the client/position an the usernames of the current object in the loop is the same
		if (request.body.username == users[i].username) {

			//changes the password of the user found by the users[i].password
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated!`
			break;
		} else {
			message = "User does not exist"
		}
	}
	response.send(message);
})



/*
	- Tells our server to listen to the port
	- If the port is accessed, we run the server
	- Returns a message to confirm tht the server is running in the terminal
*/
app.listen(port, () => console.log(`Server running at port ${port}`))